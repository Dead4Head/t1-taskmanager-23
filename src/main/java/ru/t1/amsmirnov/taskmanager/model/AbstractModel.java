package ru.t1.amsmirnov.taskmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public abstract class AbstractModel {

    @Getter
    @Setter
    @NotNull
    protected String id = UUID.randomUUID().toString();

}
