package ru.t1.amsmirnov.taskmanager.exception.entity;

public final class ModelNotFoundException extends AbstractEntityNotFoundException {

    public ModelNotFoundException() {
        super("Error! Module not found...");
    }

}
